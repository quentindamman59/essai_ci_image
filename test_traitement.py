import unittest
from traitement import create_randomfile, count_line, get_words, sorting_words_list, load_sorted_list, check_file_different

class TestTraitement(unittest.TestCase):

    def test_traitement_random_file(self):

        # Création du fichier
        response_file = create_randomfile()
        # verification de la réponse True pour le fichier créer
        self.assertEqual(response_file, True)

        # Récupération du nombre de ligne
        numberOfLine = count_line()
        # verification du nombre de ligne
        self.assertEqual(numberOfLine, count_line())

        # Récupération des mots sous formes de liste
        words_list = get_words()
        self.assertEqual(len(words_list), numberOfLine)

        # Récupération des mots
        word_list_sorted = sorting_words_list(words_list)
        self.assertEqual(len(word_list_sorted), numberOfLine)

        # Chargement de la liste trié dans un nouveau fichier
        response_file2 = load_sorted_list(word_list_sorted, numberOfLine)
        self.assertEqual(response_file2, True)

        # Vérification des fichiers, taille
        response_check = check_file_different()
        self.assertEqual(response_check, False)

if __name__ == '__main__':
    unittest.main()